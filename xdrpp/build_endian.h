//! Default value set on build machine, but can be overridden (by
//! defining WORDS_BIGENDIAN to 0 or 1) in case of cross-compilation.
#define XDRPP_WORDS_BIGENDIAN 0
