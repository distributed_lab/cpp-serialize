# Cpp Serialize

Library for marshaling/unmarshaling c++ structs to/from bytes by xdr rules.

## Usage

Must be used with classes which implements `xdr_abstract` interface.
Use gitlab.com/tokend/xdrpp to generated appropriate classes.