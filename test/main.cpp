#include <iostream>
#include <zconf.h>
#include "../src/unmarshaler.h"
#include "types.h"
#include "../src/types.h"
#include "../src/types.h"
#include "../src/marshaler.h"
#include "../src/marshaler.t.hpp"
#include "../src/unmarshaler.t.hpp"
#include "src/measurer.h"
#include "src/measurer.t.hpp"
#include "test/ledger-entries-reviewable-request.h"

using namespace xdr;
//using ::operator==;
using namespace stellar;
using namespace xdr;

int checkArrays() 
{
    xdr::xarray<xvector<xstring<13>, 3>, 5> arrayData;
    for (auto& vec : arrayData) 
    {
        for (int i = 0; i < 3; i ++) 
        {
            vec.emplace_back("1234567890123");
        }
    }

    // string oveflow
    {
        auto tmpData = arrayData;
        tmpData[0][0] = ("12345678901234");
        measurer meas;
        try 
        {
            meas.count_size(tmpData);
            std::vector<uint8_t> arrayDataBytes(meas.get_size());
            marshaler m(arrayDataBytes.data(), arrayDataBytes.data() + meas.get_size());
            m.to_bytes(tmpData);
            m.done();
        }
        catch (std::exception& e)
        {
            std::cout << "success cought vector overflow " << e.what() << std::endl;
        }    
    }

    // vector overflow
    {
        auto tmpData = arrayData;
        tmpData[0].emplace_back("12345678987654321");
        measurer meas;
        try 
        {
            meas.count_size(tmpData);
            std::vector<uint8_t> arrayDataBytes(meas.get_size());
            marshaler m(arrayDataBytes.data(), arrayDataBytes.data() + meas.get_size());
            m.to_bytes(tmpData);
            m.done();
        }
        catch (std::exception& e)
        {
            std::cout << "success cought vector overflow " << e.what() << std::endl;
        }
    }

    //success
    measurer meas;
    meas.count_size(arrayData);
    std::vector<uint8_t> arrayDataBytes(meas.get_size());
    marshaler m(arrayDataBytes.data(), arrayDataBytes.data() + meas.get_size());
    m.to_bytes(arrayData);

    m.done();

    // use output to decode with another lib, with golang lib it works
    // for (auto byte : arrayDataBytes) 
    // {
    //     std::cout << uint32_t(byte) << " ";
    // }
    // std::cout << std::endl;

    xdr::xarray<xvector<xstring<13>, 3>, 5> parsedData;

    unmarshaler unm(arrayDataBytes.data(), arrayDataBytes.data() + arrayDataBytes.size());
    unm.from_bytes(parsedData);
    unm.done();

    for (int j = 0; j < 3; j++) 
    {
        for (int i = 0; i < 3; i ++) 
        {
            if (parsedData[j][i] != "1234567890123") 
            {
                throw std::runtime_error("Unexpected string value");
            }
        }
    }

    std::cout << "success arrays unmarshaling and marshaling" << std::endl;

    return 0;
}

int checkUnions() 
{
    ReviewableRequestEntry::_body_t body(ReviewableRequestType::CREATE_SALE);
    try 
    {
        body.preIssuanceRequest();
    } 
    catch (std::exception& e)
    {
        std::cout << "success checking " << e.what() << std::endl;
    }

    xdr::xvector<SaleCreationRequestQuoteAsset> quoteAssets = {SaleCreationRequestQuoteAsset("USD", 10000000, LedgerVersion::EMPTY_VERSION)};
    for (int i = 0; i < 99; i++) 
    {
        quoteAssets.emplace_back(SaleCreationRequestQuoteAsset("BTC", 1000, LedgerVersion::EMPTY_VERSION));
    }

    body.saleCreationRequest().quoteAssets.append(quoteAssets.data(), quoteAssets.size());
    body.saleCreationRequest().creatorDetails = "Some details";
    body.saleCreationRequest().defaultQuoteAsset = "USD";
    body.saleCreationRequest().saleType = UINT64_MAX;
    body.saleCreationRequest().startTime = 1563179523;
    body.saleCreationRequest().endTime = 1563179524;
    body.saleCreationRequest().softCap = 100000000;
    body.saleCreationRequest().hardCap = 100000000;
    body.saleCreationRequest().baseAsset = "BTC";
    body.saleCreationRequest().requiredBaseAssetForHardCap = 50000000;
    body.saleCreationRequest().saleTypeExt.saleType(SaleType::BASIC_SALE);

    measurer meas;
    meas.count_size(body);
    std::vector<uint8_t> raw(meas.get_size());
    marshaler m(raw.data(), raw.data() + raw.size());
    m.to_bytes(body);
    m.done();

    ReviewableRequestEntry::_body_t bodyCopy;

    unmarshaler u(raw.data(), raw.data() + raw.size());
    u.from_bytes(bodyCopy);
    u.done();

    if (!(body == bodyCopy)) 
    {
        throw std::runtime_error("Expected bodies to be equal");
    }

    std::cout << "success union switch marshaling and unmarshaling" << std::endl;

    return 0;
}

// parse reviewable request entry from bytes
// then try parse another row to the same object
int checkStructs()
{
    std::string rawBytes = "0 0 0 0 0 0 1 17 74 66 50 96 146 227 25 50 136 221 88 156 239 26 221 146 195 88 168 175 230 0 108 26 237 213 88 147 131 170 3 137 0 0 0 0 81 57 122 228 218 177 21 124 231 33 18 14 115 25 205 120 135 31 158 251 233 54 155 20 173 41 251 115 173 146 85 16 0 0 0 15 65 117 116 111 116 101 115 116 32 114 101 106 101 99 116 0 0 0 0 0 65 194 95 155 102 187 71 215 150 65 61 24 178 14 44 237 124 230 40 121 64 142 213 248 246 117 154 214 245 182 124 210 0 0 0 1 0 0 0 64 49 55 101 98 55 48 48 51 52 98 53 98 55 49 48 57 50 53 50 49 100 49 56 52 99 53 101 55 98 48 54 57 100 52 55 100 101 54 53 55 101 53 49 97 101 102 50 98 101 49 49 97 48 48 99 49 49 53 48 51 54 57 52 51 0 0 0 0 92 231 241 73 0 0 0 8 0 0 0 0 81 57 122 228 218 177 21 124 231 33 18 14 115 25 205 120 135 31 158 251 233 54 155 20 173 41 251 115 173 146 85 16 0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 66 123 34 98 108 111 98 95 105 100 34 58 34 66 67 81 82 66 70 70 90 51 88 73 89 89 52 72 54 54 90 86 70 68 70 77 73 85 79 83 67 84 53 71 71 74 68 73 83 70 89 51 81 70 54 85 79 68 73 86 51 51 73 73 81 34 125 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 2 123 125 0 0 0 0 0 0";
    std::vector<uint8_t> vec;
    std::istringstream is( rawBytes );
    uint32_t x;
    while (is >> x)
    {
        vec.emplace_back(uint8_t(x));
    }

    unmarshaler u(vec.data(), vec.data() + vec.size());
    ReviewableRequestEntry actual;
    u.from_bytes(actual);
    u.done();

    if (actual.requestID != 273) 
    {
        throw std::runtime_error("expected request id to be 273");
    }

    if (actual.rejectReason != "Autotest reject") 
    {
        throw std::runtime_error("expected reject to be Autotest reject");
    }

    if (actual.createdAt != 1558704457)  
    {
        throw std::runtime_error("expected createdAt to be 1558704457");
    }

    if (actual.tasks.allTasks != 1) 
    {
        throw std::runtime_error("expected tasks to be 1");
    }

    if (!actual.reference) 
    {
        throw std::runtime_error("expected reference to be not null");
    }

    if (*actual.reference != "17eb70034b5b71092521d184c5e7b069d47de657e51aef2be11a00c115036943") 
    {
        throw std::runtime_error("expected reference to be valid string");
    }

    measurer meas;
    meas.count_size(actual);
    std::vector<uint8_t> bytes(meas.get_size());
    marshaler m(bytes.data(), bytes.data() + bytes.size());
    m.to_bytes(actual);

    m.done();

    for (int i = 0; i < vec.size(); i++) 
    {
        if (vec[i] != bytes[i]) 
        {
            std::cout << int(vec[i]) << "  " << int(bytes[i]) << std::endl;
            throw std::runtime_error("Expected bytes to be equal");
        }
    }

    rawBytes = "0 0 0 0 0 0 1 21 197 81 62 121 82 129 72 167 203 139 196 168 241 188 233 227 249 201 6 21 197 32 120 188 7 51 0 192 144 102 204 17 0 0 0 0 226 231 58 183 11 243 52 5 60 21 199 82 199 58 10 121 110 61 132 205 210 213 112 15 42 199 149 40 20 105 212 94 0 0 0 0 0 0 0 0 65 194 95 155 102 187 71 215 150 65 61 24 178 14 44 237 124 230 40 121 64 142 213 248 246 117 154 214 245 182 124 210 0 0 0 0 0 0 0 0 92 231 241 134 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 13 49 53 53 56 55 48 52 50 53 48 50 55 52 0 0 0 0 0 0 4 68 79 71 69 0 0 0 0 92 231 241 216 0 0 0 0 92 231 255 112 0 0 0 0 0 15 66 64 0 0 0 0 0 30 132 128 0 0 1 79 123 34 110 97 109 101 34 58 34 65 117 116 111 116 101 115 116 32 83 97 108 101 32 102 114 111 109 32 117 115 101 114 32 117 49 53 53 56 55 48 52 50 53 55 54 50 57 64 109 97 105 108 46 99 111 109 34 44 34 115 104 111 114 116 95 100 101 115 99 114 105 112 116 105 111 110 34 58 34 65 117 116 111 116 101 115 116 32 83 97 108 101 34 44 34 100 101 115 99 114 105 112 116 105 111 110 34 58 34 85 80 65 75 82 87 51 86 71 67 84 90 65 51 88 89 69 75 90 76 51 79 50 83 87 85 76 54 82 66 73 82 51 71 72 79 72 73 85 52 87 73 52 53 86 77 87 55 68 75 71 65 34 44 34 108 111 103 111 34 58 123 34 109 105 109 101 95 116 121 112 101 34 58 34 105 109 97 103 101 47 106 112 101 103 34 44 34 110 97 109 101 34 58 34 108 111 103 111 49 46 106 112 103 34 44 34 107 101 121 34 58 34 100 112 117 114 101 120 52 105 110 103 52 50 112 104 115 118 112 53 107 109 110 117 104 115 104 114 106 121 112 106 105 97 50 107 103 117 118 117 50 109 118 98 109 111 122 113 118 106 50 51 110 101 115 109 106 109 118 106 53 108 107 111 99 105 97 118 121 99 115 53 122 105 114 103 102 108 97 108 122 111 102 113 121 50 108 100 106 97 34 125 44 34 121 111 117 116 117 98 101 95 118 105 100 101 111 95 105 100 34 58 34 34 125 0 0 0 0 3 0 0 0 0 0 0 0 0 0 15 66 64 0 0 0 0 0 0 0 1 0 0 0 3 85 83 68 0 0 0 0 0 0 15 66 64 0 0 0 0 0 0 0 4 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0";
    is = std::istringstream( rawBytes );
    vec.clear();
    while (is >> x)
    {
        vec.emplace_back(uint8_t(x));
    }
    unmarshaler u1(vec.data(), vec.data() + vec.size());

    u1.from_bytes(actual);
    u1.done();

    if (actual.rejectReason != "") 
    {
        throw std::runtime_error("expected reject reason to be empty");
    }

    if (actual.reference != nullptr) 
    {
        throw std::runtime_error("expected reference to be null");
    }

    std::cout << "success reviewable request unmarshaling and marshaling" << std::endl;

}

int main()
{
    checkStructs();
    checkArrays();
    checkUnions();

    return 0;
}

