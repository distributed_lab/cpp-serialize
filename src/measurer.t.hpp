#include "measurer.h"
#include <iostream>

namespace xdr
{
template <typename T>
void measurer::count_size(const std::unique_ptr<T> &v)
{
    current += 4;

    if (!v)
    {
        return;
    }

    count_size(*v);
}

template<typename T, uint32_t N>
void
measurer::count_size(xdr::xvector<T, N> const& value)
{
    current += 4;

    if (value.size() > N) 
    {
        std::cout << "actual size: " << value.size() << ", maxSize: " << N << std::endl;
        throw std::runtime_error("data vector size overflow");
    }

    for (T const & elem : value)
    {
        count_size(elem);
    }
}

template <uint32_t N>
void
measurer::count_size(xdr::xarray<uint8_t, N> const& value)
{
    uint32_t extraBytes = (4 - (N % 4)) % 4;

    current += N + extraBytes;
}

template <typename T, uint32_t N>
void
measurer::count_size(xdr::xarray<T, N> const& value)
{
    for (T const & elem : value)
    {
        count_size(elem);
    }
}

template<uint32_t N>
void
measurer::count_size(xdr::xvector<uint8_t, N> const& value)
{
    size_t actualSize = value.size();

    if (actualSize > N) 
    {
        std::cout << "actual size: " << actualSize << ", maxSize: " << N << std::endl;
        throw std::runtime_error("byte vector size overflow");
    }

    uint32_t extraSize = (4 - (actualSize % 4)) % 4;

    current += actualSize + extraSize + 4;
}

template<uint32_t N>
void
measurer::count_size(xdr::xstring<N> const& value)
{
    size_t actualSize = value.size();

    if (actualSize > N) 
    {
        std::cout << "actual size: " << actualSize << ", maxSize: " << N << std::endl;
        throw std::runtime_error("string size overflow");
    }

    uint32_t extraSize = (4 - (actualSize % 4)) % 4;

    current += actualSize + extraSize + 4;
}
}