
#include "unmarshaler.h"

namespace xdr
{

template<typename T, uint32_t bytesNum>
bool
unmarshaler::from_bytes(xdr::xarray<T, bytesNum>& value)
{
    for (auto& elem : value)
    {
        if (!from_bytes(elem))
        {
            error << "failed to fill array elem ";
            return false;
        }
    }

    return true;
}

template<uint32_t bytesNum>
bool
unmarshaler::from_bytes(xdr::xarray<uint8_t, bytesNum>& value)
{
    if (bytesNum == 0)
    {
        return true;
    }

    uint32_t extraBytes = (4 - (bytesNum % 4)) % 4;
    uint32_t totalBytes = bytesNum + extraBytes;

    if (totalBytes > INT32_MAX)
    {
        error << "total bytes overflow ";
        return false;
    }

    if (totalBytes > (end - current))
    {
        error << "not enough bytes ";
        return false;
    }

    for (uint8_t& elem : value)
    {
        elem = *current;
        current++;
    }

    for (int i = 0; i < extraBytes; i++)
    {
        if (extraBytes != 0x00)
        {
            error << "non zero extra byte ";
            return false;
        }

        current++;
    }

    return true;
}

template<typename T, uint32_t N>
bool
unmarshaler::from_bytes(xdr::xvector<T, N>& value)
{
    uint32_t dataLen;
    if (!from_bytes(dataLen))
    {
        error << "failed to fill vector length ";
        return false;
    }

    if (dataLen > INT32_MAX)
    {
        error << "data length overflow ";
        return false;
    }

    if (dataLen > N) 
    {
        error << "data length exceedes vector max length ";
        return false;
    }

    value.clear();

    for (uint32_t i = 0; i < dataLen; i++)
    {
        T elem;
        if (!from_bytes(elem))
        {
            error << "failed to fill vector elem ";
            return false;
        }

        value.emplace_back(elem);
    }

    return true;
}

template<uint32_t N>
bool
unmarshaler::from_bytes(xdr::xvector<uint8_t, N>& value)
{
    uint32_t bytesNum;
    if (!from_bytes(bytesNum))
    {
        error << "failed to fill bytes number ";
        return false;
    }

    if (bytesNum > INT32_MAX)
    {
        error << "bytes number overflow ";
        return false;
    }

    if (bytesNum > N) 
    {
        error << "bytes number exceedes vector max length ";
        return false;
    }

    uint32_t extraBytes = (4 - (bytesNum % 4)) % 4;
    uint32_t totalBytes = bytesNum + extraBytes;

    if (totalBytes > INT32_MAX)
    {
        error << "total bytes overflow ";
        return false;
    }

    if (totalBytes > (end - current))
    {
        error << "not enough bytes ";
        return false;
    }

    value.clear();

    for (uint32_t i = 0; i < bytesNum; i++)
    {
        value.emplace_back(*current);
        current++;
    }

    for (uint32_t i = 0; i < extraBytes; i++)
    {
        if (*current != 0x00)
        {
            error << "non zero extra byte ";
            return false;
        }

        current++;
    }

    return true;
}

template<typename T>
bool
unmarshaler::from_bytes(std::unique_ptr<T>& value)
{
    bool present;
    if (!from_bytes(present))
    {
        error << "failed to fill bool ";
        return false;
    }

    if (!present)
    {
        value = nullptr;
        return true;
    }

    T innerValue;
    if (!from_bytes(innerValue))
    {
        error << "failed to fill inner value";
        return false;
    }

    value = std::make_unique<T>(innerValue);

    return true;
}

template<class T, class>
bool
unmarshaler::from_bytes(T& value)
{
    int32_t raw;
    if (!from_bytes(raw))
    {
        error << "failed to fill int32_t";
        return false;
    }

    value = static_cast<T>(raw);

    return true;
}


template<uint32_t N>
bool
unmarshaler::from_bytes(xdr::xstring<N> &value)
{
    xdr::xvector<uint8_t, N> result;
    if (!from_bytes(result))
    {
        error << "failed to fill vector ";
        return false;
    }

    value.assign(result.begin(), result.end());

    return true;
}

}